import yaml
import os

class Services:
    def __init__(self, filename):
        with open(filename, "r") as f:
            self.structure = yaml.load(f, Loader=yaml.Loader)

    def construct_site(self):
        "Build initial Hugo site from single YAML file. One-off."
        # ID -> full url for hugo
        global_url_lookup = {}

        for (area, area_info) in self.structure.items():
            entries = area_info.get("services") or []
            for (name, entry) in entries.items():
                global_url_lookup[name] = area + "/" + name

        for (area, area_info) in self.structure.items():
            area_dir = "content/docs/" + area
            os.makedirs(area_dir, exist_ok = True)

            with open(area_dir + "/_index.md", "w") as f:
                f.write("---\n")
                f.write("title: " + area_info['name'] + "\n")
                f.write("---\n")

            entries = area_info.get("services") or []

            for (name, entry) in entries.items():
                entry['title'] = entry['name']
                entry['date'] = "2019-01-01"
                entry.pop('name')
                desc = entry.get("desc") or "Content to follow"
                os.makedirs(area_dir + "/" + name, exist_ok=True)
                servicedeps = entry.get('servicedeps') or []
                datadeps = entry.get('datadeps') or []
                entry['servicedeps'] = [global_url_lookup[x] for x in servicedeps]
                entry['datadeps'] = [global_url_lookup[x] for x in datadeps]

                with open(area_dir + "/" + name + "/_index.md", "w") as f:
                    f.write("---\n")
                    yaml.dump(entry, f)
                    f.write("---\n")
                    f.write(desc)
                    f.write("\n")


os.makedirs("public/", exist_ok=True)

s = Services("services.yaml")
s.construct_site()
