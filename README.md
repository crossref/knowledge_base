Welcome to the internal Knowledge Base! It is an internal documentation site for use by Crossref staff, but it's open.

The site is built using Hugo, but there's some magic to create a diagram of how all of our services connect.

To get started head to <https://crossref.gitlab.io/knowledge_base/>

This is a work in progress and does not fully represent the current state of Crossref's systems.

## Editing

The site has links to edit pages directly in GitLab. This is the simplest way to contribute. Note that the master branch is protected, meaning that each change must be done via a Merge Request. This is handled automatically in the GitLab editor. Be sure to check that your edit didn't result in a breakage. If it succeeds, you may merge it yourself. 

## Running locally

### Setup

Installation of tools:

If you're on a Mac with homebrew installed, install hugo with:

    brew install hugo
    
    
To run the linter, which will validate and add any missing fields in new documents:

    brew install python3
    pip3 install --user pipenv
    pipenv install -r requirements.txt

### Running tools

Then run the site with:

    hugo serve
    
Then to run the linter:

    pipenv run python3 format.py --lint
    
