---
docs: []
title: Crossmark
---

## Description 
Crossmark gives readers quick and easy access to the current status of an item of content. With one click, you can see if content has been updated, corrected or retracted and access valuable additional metadata provided by the publisher. The 12 defined types of scholarly updates are as follows: 
- addendum
- clarification
- correction
- corrigendum
- erratum
- expression_of_concern
- new_edition
- new_version
- partial_retraction
- removal
- retraction
- withdrawal


## Status story
Crossmark launched in 2012 and had a major re-vamp (v2.0) in 2016 when we rolled out a new Crossmark button and redesigned the display box. It has not had any development since that time. 

## Visualisations

<iframe width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRSGv8QPOjmqI1XCnO8dLk1fSo455k0O2sJHNOyp-tzOcjCRfAt0pwzLzJvBgEQSfuEFy913SQuO_OE/pubchart?oid=328284620&amp;format=image"></iframe>

<iframe width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRSGv8QPOjmqI1XCnO8dLk1fSo455k0O2sJHNOyp-tzOcjCRfAt0pwzLzJvBgEQSfuEFy913SQuO_OE/pubchart?oid=1520870806&amp;format=image"></iframe>