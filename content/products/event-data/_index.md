---
docs: []
title: Event Data
---

## Description

Event Data is a metadata service that captures activity and assertions made on non-member platforms that refer to DOIs. There is very close collaboration with Datacite, which runs its own event data API and contributes, along with Crossref, to the [Scholix API](http://www.scholix.org). Event data is available through and API.

## Status story

Event Data started as a labs project in 2014 with a pilot in 2015. In July 2015 the board approved the setup of a new service. A beta was released in July 2017 and development continued until 2018. Between August 2018 and July 2020 only maintenance work was carried out. 

## Future development

Planning for stabilising and improving Event Data are currently being assess and are likely to include the following. 

Stability/reliability
 - Prepare code and REST APIs with a view to merging with the Cayenne Metadata REST API
 - Modernise the server infrastructure and improve monitoring
 - Fix scalability issues with Elastic Search
 
Product development
 - Automate updates of publisher domains
 - Outreach to interested users and write up use cases
 - Reassess communication: who to contact and education documentation

## Resources

- [User guide](https://www.eventdata.crossref.org/guide/index.html)
- [Education documentation](https://www.crossref.org/education/event-data/)
- [Crossref services pages](https://www.crossref.org/services/event-data/)
- [Product dashboard](https://sites.google.com/crossref.org/product/event-data)
- [Event data enquiries on Gitlab](https://gitlab.com/crossref/event_data_enquiries)
- [Status available on Crossref status page](https://status.crossref.org)
- [Event data working group](https://www.crossref.org/working-groups/event-data/)
- [Jupyter notebooks](https://gitlab.com/crossref/event_data_notebooks)

