---
docs: []
title: Cited-by
---

Cited-by shows how work has been received by the wider community, displaying the number of times it has been cited and linking to the citing content. Cited-by counts are publicly available, but only the member can see the details of which sources are citing their works. Members who include references in their own metadata are able to query publications that cite their content, and Cited-by counts are based on the citation counts of other Crossref members participating in Cited-by.