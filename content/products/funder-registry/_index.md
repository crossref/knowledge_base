---
docs: []
title: Funder Registry
---

The Funder Registry allows everyone to have transparency into research funding and its outcomes. It’s an open and unique registry of persistent identifiers for grant-giving organizations around the world.