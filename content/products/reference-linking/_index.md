---
docs: []
title: Reference Linking
---

Reference Linking enables researchers to follow a link from the reference list to other full-text documents, helping them to make connections and discover new things. 