---
area: distribution-querying
datadeps: []
desc: Generate reports. Small App. 
docs: []
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/old_mycrossref
sentry_url: 
servicedeps:
- infra-datacenter-mysql
- infra-datacenter-oracle
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: My Crossref
userfacing: true
---

This service is deprecated but some reports are still in use.

Reads Oracle.

Writes MySQL:
 - titleDb
 - resReport
 - crossref
 - msdbf.qs

