---
area: reports
datadeps: []
desc: ''
docs: []
lang: ''
legacy: true
packages:
- org.crossref.ds.reports.DepositReportManager
- org.crossref.qs.controllers.DepositReportController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/depreport?mode=titlesByQuarter|month|invoice|titleByMonth
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Title Search
userfacing: false
---


This report api is the same endpoint used to run quarterly invoicing but also contains reports used to support
invoice activities. The main report used by support as a verification for users who request it is the titlesByQuater report.
This is run for a specific user and quarter and it generates a csv (html available also) and it is run with:
http://localhost:8080/depreport?mode=titlesByQuarter&user=silver&month=2020-12-15&format=csv
