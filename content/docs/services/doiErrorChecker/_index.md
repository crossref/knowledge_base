---
area: distribution-querying
datadeps:
- 
desc: 
docs:
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products:
- reports
related_services: []
repo_links:
- https://gitlab.com/crossref/doi_error_db_processor
sentry_url: ''
servicedeps:
- infra-datacenter-mysql
- infra-datacenter-oracle
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: 
 - small-apps
title: DOI Error Checker
userfacing: true
---

AKA DOI Error DB processor is a daily cron task (10 AM every day) on one of the vftp servers. It reads the db written by the DOI Error Catcher (titleDB) and creates user reports from the reported DOI errors. It also looks up the DOI in Handle and Crossref, reports missing DOIs to publishers, notifies the reporter if it gets fixed (and they included their email).

Reads MySQL:
 - titleDB
 - crossref

Writes MySQL:
 - titleDB

Connects to Sugar

