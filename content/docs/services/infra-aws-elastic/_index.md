---
area: infrastructure-services
datadeps:
- event-data-query-api
desc: Search engine.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Elastic Search (AWS)
userfacing: false
---



Search engine.
