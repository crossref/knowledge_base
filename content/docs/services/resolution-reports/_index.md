---
area: reporting-monitoring
datadeps:
- ext-doi
desc: Generate reports and send to members by email.
docs: []
lang: Java
legacy: true
packages:
- org.crossref.qs.resolutionreport.DoiDataController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/internal/resolutionreport/doidata
- https://data.crossref.org/members_only/resolutionReport
products:
- metadata-retrieval
related_services: []
repo_links:
- https://gitlab.com/crossref/resolution_report.git
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- get-doi-db-record
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: 
 - small-apps
title: Resolution reports
userfacing: true
---

Cron (10 AM every day), populates resReport DB

TL;DR: Generate reports and send to members by email.

CNRI sends us log data for all the prefixes for which we are the RA (as seen from the Handle system). This data is processed against the Oracle database. Then the report queries the email recipient API built into Sugar to determine who to send reports to. If it does not get a recipient list from that API, the report logs prefixes whose emails have failed. 


Updated for 6/2020 run to remove crawler ip filters and update messaging.

Reads MySQL
 - titleDb
 - crossref
 - resReport

Writes MySQL:
 - resReport

