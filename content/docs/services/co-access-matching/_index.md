---
area: etl
datadeps:
- cddb
desc: Match and link DOIs for co-access status.
docs: []
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- matching
title: Co-access matching
userfacing: false
---



Match and link DOIs for co-access status.
