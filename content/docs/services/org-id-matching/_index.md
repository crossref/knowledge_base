---
area: tools-libraries
datadeps: []
desc: Sample code to explore matching of affiliation data from scientific papers against
  various databases of institutional identifiers
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://github.com/CrossRef/org-id-matching
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- matching
title: Organisation ID Matching
userfacing: true
---



Sample code to explore matching of affiliation data from scientific papers against various databases of institutional identifiers
