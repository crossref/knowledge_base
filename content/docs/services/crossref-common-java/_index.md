---
area: tools-libraries
datadeps: []
desc: Java Library for common functions.
docs: []
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://github.com/CrossRef/cr-common
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Crossref Common (Java)
userfacing: false
---



Java Library for common functions.
