---
area: infrastructure-apps
datadeps: []
desc: Communicate with Handle server.
docs: []
lang: ''
legacy: true
packages:
- org.crossref.ds.handle.HandleManager
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- ext-doi
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Handle Manager
userfacing: false
---




Content to follow.

