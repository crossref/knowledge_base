---
area: infrastructure-services
datadeps: []
desc: Legacy service-managed services running directly on VMs.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- infra-datacenter
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Traditional Vanilla services in datacenter
userfacing: false
---



Legacy service-managed services running directly on VMs.
