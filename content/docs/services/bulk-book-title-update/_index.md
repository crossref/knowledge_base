---
doc_link: []
label_name: null
product: ''
tags: ''
title: Bulk Book Title Update
weight: 0
---

Content System project package for updating book titles using a CSV file as input.

The column headings allowed are:
- new title (any instance of new_title, new-title, and any uppercase variant, convert)
- new subtitle (any instance of sub_title, sub-title, and any uppercase variant, convert)
- pisbn (p isbn, p-isbn, p_isbn, and any uppercase variant convert)
- eisbn (e isbn, e-isbn, e_isbn, and any uppercase variant convert)
- bookciteid (any uppercase variant converts)
- doi (any uppercase variant converts)
- old title (any instance of old_title, old-title, current title, current_title, current-title, and any uppercase variant, convert)

Required columns:
- new title is ALWAYS required
- at least one of: bookciteid, doi, or at least one isbn (pisbn, eisbn)

Precedence:
- If more than one type of identifier (bookciteid, doi, isbn) is provided, they are attempted in a preferred order.
  1. bookciteid
  2. doi
  3. isbn(s)

   Once the title has found and updated a title, it will stop trying the next identifier.
 
 If a solid match is not made, no update is made and the skipped entry is noted in the log.
 If the existing title/subtitle matches the new title/subtitle, no update is done and it is noted in the log.

 For ISBN matching, some additional logic is included:
  - if one of the isbns matches and the other doesn't, the missing isbn is added for the title.
  - if the isbn matches but the type (e,p) differs, the isbn type is updated (i.e. file provided contains pisbn but it is found in the system as eisbn, it gets update to pisbn in the system). 
  - if duplicate isbns for a title are found, the duplcates are removed.

 
 