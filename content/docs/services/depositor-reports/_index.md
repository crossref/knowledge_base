---
area: reporting-monitoring
datadeps: []
desc: 'Java code that runs via cron on vftp.crossref.org to generate the webpage content for the 3 reports below'
docs: []
lang: ''
legacy: true
packages:
prod_heartbeats: []
prod_urls:
- https://data.crossref.org/reports/depositorJ.html
- https://data.crossref.org/reports/depositorB.html
- https://data.crossref.org/reports/depositorCP.html
products: []
related_services: []
repo_links: 
  - https://gitlab.com/crossref/deposit_report
  - https://gitlab.com/crossref/deposit_report_book
  - https://gitlab.com/crossref/deposit_report_confproc
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Depositor Reports
userfacing: true
---

Deposit Report: Cron (9 PM every Monday), generates conflictReport.html and 10.*_conflicts.xml, uploads them to FTP

Deposit Report (book): Cron (2 AM every Tuesday), generates depositReportB.html and uploads it to FTP

depositReport (confproc): Is run by the same cron script as Book report, generates depositReportCP.html and uploads it to FTP


The repos:
- https://gitlab.com/crossref/deposit_report
- https://gitlab.com/crossref/deposit_report_book
- https://gitlab.com/crossref/deposit_report_confproc

contain the java code and xslt scripts that run in weekly cron jobs on vftp that generate the XML for the reports. The shell script that runs the jobs then runs xalan against the XML via the XSLT scripts to produce the HTML report contents which are then FTPed to webhost where the content is included in an import statement in the actual HTML pages that the report URLs point at.

Reads Oracle / PostgreSQL

Reads MySQL:
 - titleDB

Writes MySQL:
 - titleDB

Uses FTP / Filesystem.

Uses email.

Uses SugarCRM.