---
area: infrastructure-apps
datadeps: []
desc: ''
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: 
 - https://gitlab.com/crossref/title-db-builder
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Title DB Builder
userfacing: false
---

Cron (1 AM every day), generates titles.csv and optionally uploads them to FTP, populates titleDB

Reads Oracle / PostgreSQL

Reads MySQL:
 - titleDB

Writes MySQL:
 - titleDB

Uses FTP / File System

