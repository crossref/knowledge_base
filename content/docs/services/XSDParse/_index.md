---
area: distribution-querying
datadeps: []
desc: 
docs:
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products:
- reports
related_services: []
repo_links:
- https://gitlab.com/crossref/xsd_parse
sentry_url: ''
servicedeps:
- infra-datacenter-mysql
- infra-datacenter-oracle
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: 
 - small-apps
title: XSD Parse
userfacing: true
---

