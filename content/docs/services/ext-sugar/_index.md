---
area: external
datadeps:
- member-info
desc: SugarCRM. System for managing members' accounts.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls:
- https://crossref.sugarondemand.com
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls:
- https://crossreftest.sugarondemand.com
tags: []
title: Sugar CRM
userfacing: true
---

SugarCRM. System for managing members' accounts.

This is administered by Crossref staff. We work with Faye Systems to administer our Sugar installation.

Member accounts are managed here. Data is synchronised between Sugar and CS via MemberInfo, in both directions.

Data is sent from Sugar to our Intacct financial system when new accounts are set up in Sugar that require billing.

Sync processes run between Sugar and various other systems on a schedule.
- memberinfo build syncs with sugar at 7am, 12pm, 4pm, and 8pm daily.
- last deposit dates in sugar are updated daily at 5pm.
- doi counts are updated in sugar at 6pm daily.
- Act-On integration syncs with sugar daily at 7pm.
- Intacct syncs accounts and invoices hourly

There is more documentation in this [google doc](https://docs.google.com/document/d/1S7q61bBVqpLuezFO6k2YuRo80NDjD_3dPfxbb_wQANQ/edit?tab=t.0)