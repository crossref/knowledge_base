---
area: distribution-querying
datadeps: []
desc: 
docs:
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products:
- reports
related_services: []
repo_links:
- https://gitlab.com/crossref/new_doi_crawler
sentry_url: ''
servicedeps:
- infra-datacenter-mysql
- infra-datacenter-oracle
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: 
 - small-apps
title: New DOI Crawler
userfacing: true
---

Only started by crawlLauncher

Reads MySQL:
 - titleDB

Writes MySQL:
 - titleDB

