---
area: etl
datadeps:
- open-funder-registry
desc: Matches Funder IDs within deposits.
docs: []
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products:
- funder-registry
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- matching
title: Funding Data Matching
userfacing: false
---



Matches Funder IDs within deposits.
