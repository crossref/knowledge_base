---
area: external
datadeps:
- deposit-billing
desc: Billing file supplied annually by TurnItIn
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: TurnItIn Billing File
userfacing: false
---



Billing file supplied annually by TurnItIn
