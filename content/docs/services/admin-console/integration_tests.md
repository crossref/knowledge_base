---
related_services: []
repo_links: []
tags:
- admin console
- cypress integration tests
title: Cypress Integration Tests
weight: 0
---

Integration/E2E tests are available using the [Cypress](https://www.cypress.io/) framework.

Tests are located under the `cypress/integration` directory.

Supporting functions and custom commands live in `cypress/support`.

The tests can be run using `./run_cypress.sh` for headless running, `./run_cypress.sh -g` for a headed runner, in the project root.

This will start a docker container using the correct configuration for a Mac OS or linux host.

Environment variables passed into Cypress are defined in `cypress.json`.

Secret values can be specified in `cypress.env.json` which will override values in `cypress.json`.
