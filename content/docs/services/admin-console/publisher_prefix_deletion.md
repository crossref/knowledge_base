---
related_services: []
repo_links: []
tags:
- admin console
- publisher prefix
title: Removal of the Publisher Prefix (Admin Tool)
weight: 0
---
Prefixes are periodically allocated to new members through the application process. Sometimes this is done erroneously and the prefix needs to be removed from the publisher. 
However, like doi's, we can't actually delete the prefix or the publisher id. Instead, we are marking the publisher id (state = 1) as deleted and appending the doi prefix with the 'deleted_' tag in the database. This makes the prefix available to be assigned and used again in the future.

`Deletion Requirements:`

* In order to delete a publisher prefix, the publisher in question cannot own the following:
    * DOI
    * Journal Title
    * Book Title

`How to delete the publisher prefix:`

        * Admin Tool (root user)
        * Users > PUBLISHER (tab)
        * Choose a method to search for the publisher (i.e. DOI PREFIX)
        * Modify > DELETE

    * Note: No prompt will be given. So, be sure you want to delete this publisher's prefix before selecting.

`Undelete:`

* If a publisher prefix was deleted in error, it can be restored by following the Deletion Steps (above). The DELETE button will be replaced with an UNDELETE button instead. Pressing this will remove the deleted_ from the prefix and change the state back to zero.
    * Note: This will only work as long as the doi prefix hasn't been assigned to another publisher.   