---
area: infrastructure-apps
datadeps:
- deposit-queue
desc: Admin console. For use by staff and members.
docs: []
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products:
- cited-by
- crossmark
- funder-registry
- metadata-plus
- metadata-retrieval
- metadata-search
- orcid-auto-update
- reference-linking
- similarity-check
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-oracle
- infra-datacenter-activemq
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Admin console
userfacing: true
---



Admin console. For use by staff and members.
