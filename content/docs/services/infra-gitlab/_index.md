---
area: infrastructure-services
datadeps: []
desc: GitLab as a platform for hosting files, running CI or issue tracking.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: GitLab
userfacing: false
---



GitLab as a platform for hosting files, running CI or issue tracking.
