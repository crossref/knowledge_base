---
area: resolution
datadeps: []
desc: Display page for multiple-resolution and co-access
docs:
- https://gitlab.com/crossref/chooser
lang: Python
legacy: true
packages: []
prod_heartbeats:
- https://chooser.crossref.org/heartbeat
prod_urls:
- https://chooser.crossref.org/
products:
- chooser
related_services: []
repo_links:
- https://gitlab.com/crossref/chooser
sentry_url: 'https://crossref.sentry.io/issues/?project=4504218356875264'
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Chooser
userfacing: true
---


“Chooser” is a FastAPI application written in Python that performs the following functions:

* Presents options, with clearbit logos, for multiple resolution items
* Presents options, with clearbit logos, for co-access items

The HANDLE system determines whether an item should be routed to internal systems, as per the “redirect sequence”, below.

## Redirect Sequence
How does HANDLE know to redirect MR/Co-access requests to the internal Crossref system? Because the CS system intercepts deposits that are multiple resolution or coaccess and points them to iPage at the time of deposit.

Incoming DOI/HANDLE request points the DOI to https://www.crossref.org/iPage?doi=10.XXXX which in turn rewrites to Chooser with the DC HAProxy redirecting these iPage requests to Chooser. Requests to coaccess (apps.crossref.org/coaccess) or multiple resolution (doi.crossref.org/iPage / mr.crossref.org / \*.crossref.org/iPage) get redirected to Chooser.


## Data Sources
The Chooser interface makes calls to two separate data sources:

* The REST API to gather metadata about the current item;
* and the Clearbit service to gather logos for off-site links


## Error Handling
Chooser uses Sentry for debugging and error handling. Until May 2024 the most frequent cause of failure was a timeout on Clearbit logo resolution. Now the most frequent error is a retry error that occurs in sync with failures of the REST API.
