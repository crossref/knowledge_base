---
related_services: []
repo_links: []
tags:
- clinical-trial-numbers
title: Clinical Trial Schema
weight: 0
---



The Clinical Trial Number schema represents the links to clinical trial numbers from e.g. journal artcles.
