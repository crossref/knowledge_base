---
area: reporting-monitoring
datadeps:
- deposit-events
desc: Generate quarterly billing files for upload to Intacct system.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products:
- content-registration
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Deposit Billing
userfacing: false
---



Generate quarterly billing files for upload to Intacct system.
