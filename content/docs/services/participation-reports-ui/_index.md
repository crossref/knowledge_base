---
area: reporting-monitoring
datadeps:
- participation-reports-middleware
desc: Participation Reports web UI. Served as part of the website.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products:
- participation-reports
related_services: []
repo_links:
- https://github.com/CrossRef/Prep
sentry_url: ''
servicedeps:
- participation-reports-middleware
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Participation Reports UI
userfacing: true
---



Participation Reports web UI. Served as part of the website.
