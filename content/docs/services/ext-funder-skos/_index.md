---
area: external
datadeps: []
desc: Funder Registry SKOS RDF file from Elsevier.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Funder Registry SKOS
userfacing: false
---



Funder Registry SKOS RDF file from Elsevier.
