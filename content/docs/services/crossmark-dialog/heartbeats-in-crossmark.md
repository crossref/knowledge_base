---
related_services: []
repo_links: []
tags:
- crossmark
- heartbeat
title: Heartbeats in Crossmark
weight: 0
---




The Crossmark Dialog serves a simple heartbeat. It is exposed at:

 - <https://crossmark.crossref.org/heartbeat>

It returns:

 - Hostname of machine
 - Software version
 - Status

