---
area: distribution-querying
datadeps: []
desc: handles forward link query requests for a given doi
docs: []
lang: ''
legacy: true
packages:
- org.crossref.qs.controllers.ForwardLinkQueryController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/servlet/getForwardLinks/**
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- authorization-service
- citation-reference-finder
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Forward Link Query
userfacing: false
---





