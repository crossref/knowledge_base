---
area: distribution-querying
datadeps: []
desc: Returns a citation reference for the given DOI or citation-id.
docs: []
lang: ''
legacy: true
packages:
- org.crossref.qs.citationreference.CitationReferenceFinder
- org.crossref.qs.controllers.GetCitationReferenceBrecordController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/internal/cr
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Citation Reference Finder
userfacing: false
---




Content to follow.

