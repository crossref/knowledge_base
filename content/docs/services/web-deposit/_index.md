---
area: metatdata-deposit
datadeps:
- cddb
desc: ''
docs: []
lang: Java
legacy: true
packages:
- webDeposit
prod_heartbeats: []
prod_urls:
- https://apps.crossref.org/webDeposit
- https://www.crossref.org/webDeposit/**
products:
- metadata-deposit
related_services: []
repo_links:
- https://gitlab.com/crossref/web_deposit
sentry_url: ''
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- web-deposit
- small-apps
title: Web Deposit
userfacing: true
---


webDeposit is a tool to allow end users to deposit data via a step-by-step form entry system. It supports journals, book, conferences, reports, dissertations, crossmark policy pages, NLM (JATS) files, csv resource files. On 10/15/2020, optional abstracts were added to all content types.
