---
related_services: []
repo_links: []
tags: []
title: Queue server operations
weight: 0
---





## Admin UI operations:

### Suspend/Enable Queue
  Clicking the link to enable or suspend the queue will signal the queuserver to start or stop giving out queued submissions (both MD and Query) to waiting deposit processors. 
  Items currently in process will not be canceled, and deposits and queue will continue as before. 
  
### Reconstruct Queue 
  The queue stores things in memory and is notified of things being processed or added to the queue via the processors and the admins. If there's a disruption, like restarting the queue, 
  the submissions will be loaded from the database. Submissions are loaded based on their received and completed times. The default look back is **3 weeks**. 
  The Reconstruct button will do this operation without having to restart the queue server. **NOTE:** it requires the queue to be stopped and no submissions being processed. 
  
### System control
  The enable/disable upload notifications links sends messages to registered CS servers that take uploads to change their submissions handling. Specifically, if the notifications are disabled, it will also disable writing the submissions information to the database. 
  Instead, the customer uploaded files will accumulate on those machines (typically svc1a and svc1b) until the notifications are enabled. At which point, the files will be read and both updated in the database (and sent to S3) and the queueserver will be notified. 
  
