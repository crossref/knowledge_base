---
area: collection
datadeps:
- oai-pmh
desc: Collection of Data Citations from Crossref
docs: []
lang: Clojure
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
 - https://github.com/CrossRef/clj-harvest
sentry_url: ''
servicedeps:
- event-data-event-bus
- infra-hetzner-docker
- oai-pmh
- infra-aws-cloudwatch
- infra-hetzner-elk
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Decanter Data Citation
userfacing: false
---



Collection of Data Citations from Crossref
