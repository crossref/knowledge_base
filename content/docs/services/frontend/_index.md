---
area: tools-libraries
components:
- Meta
datadeps: []
desc: Crossref Front End Framework
docs: []
lang: Vue
legacy: false
packages: []
prod_heartbeats: []
prod_urls:
- https://crossref.gitlab.io/frontend
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/frontend
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Crossref Front End Framework
userfacing: true
---

Our framework for building new browser-based user interfaces. Built with [Vue.js](https://vuejs.org/) framework, on [Vuetify](https://vuetifyjs.com/en/) component framework. New components can be built within this framework and then included into places where needed.

## Project Boilerplate
For the latest boilerplate for frontend code, head to the [MyCrossref repo](https://gitlab.com/crossref/mycrossref/) on Gitlab, and checkout either `crossref-template` for a project with Vue, Vuetify, Jest, Cypress and ESLint setup, or `crossref-template-json-forms` for the same but with JSON Forms intergated too.


## Targetted Browsers

We follow Vuetify's [browser support policy](https://vuetifyjs.com/en/getting-started/browser-support), which is currently:

| Browser                         | Support Status |
| ------------------------------- | -------------- |
| Chromium (Chrome, Edge Insider) | Supported |
| Edge                            | Supported |
| Firefox                         | Supported |     
| Safari 10+  	                  | Supported |
| IE11/Safari 9	                  | Supported with polyfill |
| IE 10 and below                 | Not Supported |
| Safari 8 and below              | Not Supported |

We include the polyfill for IE11/Safari 9, and note the [caveat](https://vuetifyjs.com/en/getting-started/browser-support/#template-caveat) relating to IE 11 `<template>` tag support.

## Coding Style

New code should be [written in typescript]({{< relref "typescript.md" >}}) and should adhere to the [Typescript Standard Style](https://standardjs.com/#typescript). When extending existing Javascript with new features, those new features should be written in Typescript, and the entire module considered for migration to Typescript at the same time.

JavaScript should be written to [ES6 (ECMAScript 2015)](https://en.wikipedia.org/wiki/ECMAScript#ES2015) specification where possible, and transpiled during a build step to code runable on all targetted browsers.
JavaScript should adhere to the Javascript Standard Style (style guide, linter and formatter).
Vue components should be built as [Single File Components](https://vuejs.org/v2/guide/single-file-components.html).
Code should be transpiled using [Babel](https://babeljs.io/) and bundled (built) with [webpack](https://webpack.js.org/).

## Linting

Code can be linted with [ESLint](https://eslint.org/), using an .eslintrc configuration file that extends the "standard" ruleset. An example of this can be found in the [Frontend project repo](https://gitlab.com/crossref/frontend).

Run eslint with `eslint --ext .js,.vue src` to check all .js or .vue files within the src/ directory.

Run eslint with `eslint --fix --ext .js,.vue src` to check and fix the same

In the Frontend VueJS [project repo](https://gitlab.com/crossref/frontend), the vue CLI service lint command is configured to run ESLint with `--fix` enabled against the Standard Style ruleset, and an extra ruleset that checks for deviations from the recommended VueJS component style guide.

Run Vue CLI lint with `npm run lint` to check and fix all .js and .vue files within the Vue project.

When running the project with `npm run serve`, lint on save is enabled by default. This can be [controlled](https://cli.vuejs.org/config/#lintonsave) by the `lintOnSave` paramter within `vue.config.js`.

## Testing

Unit tests should be written to target the [Jest](https://jestjs.io/) test runner.

Unit tests targetting Vue components should use the [Vue Test Utils](https://vue-test-utils.vuejs.org/).

Intergration tests should be written to for [Cypress](https://www.cypress.io/). 

## Mocking endpoints

Endpoint responses can be mocked using Jest's [manual mocks](https://jestjs.io/docs/en/manual-mocks), a lightweight service like [json-server](https://github.com/typicode/json-server) or [MirageJS](https://miragejs.com/).

When creating manual mocks, use `mockResolvedValue` for returning simple values, or `mockImplementation` for more complex cases. 

Json-server and MirageJS let you fake an entire API, and use it during development as well as while running tests (including [Cypress](https://www.cypress.io/) tests). MirageJS has the advantage of running entirely within the app (it hijacks XHRs) and json-server has the advantage that it simulate the real network conditions of requesting an API exposed on a different domain/port (which allows you to test things like CORS).

## Scoped CSS
Vuetify applies an aggressive [CSS reset stylesheet](https://vuetifyjs.com/en/styles/css-reset/) gloablly, and this can cause trouble when non-vuetify content must co-exist on the same page. For example, inputs are rendered borderless, and all padding and margins are reset. This is only a live problem on the webDeposit form, where the authentication UI must co-exist with the legacy form content.

Rather than un-do all the resets and attempt to override them only for the legacy content, a version of the framework CSS is built with the Vuetify CSS (including the reset) scoped with a prefix - the `data-vuetify` html attribute. Any page that needs to allow legacy content to co-exist with Vuetify components can reference `crossref-ui.prefixed.css`.

A known side-effect of this prefixed CSS is that it causes extra an vertical scroll bar (as well as the native browser scrollbar) to appear within the Vuetify root component, if the content is greater than the viewport height. Therefore, only use this CSS prefix workaround when necessary.

## Environment-specific configuration

Some configuration properties or values requried by the framework will differ, based on which environment it is deployed into.

As the same code is deployed into sandbox and production, the framework performs automated detection of the deployed environment, based on the URL it is hosted at.

The urls are tested by comapring the current hostname to a list provided in the `.env` file, with their corresponding environment.
NB previously this test was only against the subdomain of the current url, hence the variables are called `VUE_APP_..._SUBDOMAINs` rather than `..._DOMAINS`. This will be fixed in a later release.
```
# comma,separated list of domains that match and environment
VUE_APP_PRODUCTION_SUBDOMAINS="doi.crossref.org,authenticator.production.crossref.org,authenticator.crossref.org"
VUE_APP_SANDBOX_SUBDOMAINS="test.crossref.org,sandbox.crossref.org,authenticator.sandbox.crossref.org"
VUE_APP_STAGING_SUBDOMAINS="staging.crossref.org,authenticator.staging.crossref.org"
VUE_APP_LOCAL_SUBDOMAINS="localhost,127.0.0.1"
```
If a non-production environment is matched, the concomitant configuration overrides file is loaded from `src/environments/nameofenvironment.js` and these properties merged in on top of the base configuration. Properties passed into the first argument of the `run({...})` method are merged in last, over the top of any environment-specific overrides.

If no environment is matched, `production` is returned as the default, and an error logged.

The domain to environment matching functionality (in `getEnv()`) is wrapped in a try/catch as it occurs before Sentry is loaded (so it can be passed into `Sentry.init({...})` as the `environment` property). Any errors are added to an array, which is processed and any exceptions shipped to Sentry after the `Sentry.init()` call.

## Integration into CS

The integration into the Admin Console is handled by (this .jsp)[https://gitlab.com/crossref/content_system/-/blob/develop/web/jsp/admin/login.jsp] and serves as a useful reference for the required configuration and HTML boilerplate.
