---
related_services: []
repo_links: []
tags: null
title: Integration testing
weight: 0
---

Use the [Cypress](https://www.cypress.io/) framework for integration testing. It can run headed (to show what the browser is doing during testing) or headless (on the command line, for example, in CI). When running headless, it can record screenshots and videos of failed tests. Running headed, it's possible to 'time travel' through the tests, and see exactly what was targeted and when.

## Use during development

For automated testing while developing, run Cypress headed (`npm run cy:open`) to automatically run a chosen test suite, picking up on code changes and re-running the tests.

## Use in CI

Cypress is configured to run headless as part of the frontend CI pipeline, after the build step. Frontend integrations are simulated in `src/integrations/...` with appropriate HTML pages, related assets, and a json-server configuration to match any targetted APIs. The CI job copies the newly built assets from `dist` into the `src/integrations/..integration_name../public/dist` directory, starts the json-server and a lightweight http server, then runs cypress against these.
Screenshots and videos of test runs are stored as artifacts in the `cypress/screenshots` and `cypress/videos` directories.

## Configuration
The default config file is `cypress.json` in the project root. Cypress can be called with a different config file by passing the `--config-file` flag.

## Environment variables
Cypress can be configured by passing environemnt variables. They can be passed on the [command line](https://docs.cypress.io/guides/guides/environment-variables.html#Option-4-env) with the `--env` flag, in the 
