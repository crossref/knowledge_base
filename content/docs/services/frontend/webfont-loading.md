---
related_services: []
repo_links: []
tags: null
title: Webfont loading
weight: 0
---

Custom webfonts can have a significantly detrimental impact on the page loading time and experience of users - particularly those on less performant or intermittent network connections. Load only the minimum font files required, and optimise their loading.

## System fonts

System fonts are those already present on a user's device, installed by default with their operating system. This is the fastest way to load fonts, and the fallback font(s) in a font stack should always be chosen from the 'web safe' system fonts. The final fallback font in a stack should always be `sans-serif` or `serif` to cover systems that have none of the named fonts installed (eg Android has no Arial - Droid Sans instead).

The font stack used by the Frontend framework is `font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;`

## Self-hosted fonts

Font files should be stored and hosted on the Crossref assets CDN, including fonts provided by third-party services such as Google Fonts and TypeKit. Self-hosting font files provides resilience against the failure of a third party service, and protection from unexpected changes of font name by a third party.

Aligned with the frontend [browser support targets]({{< ref "docs/services/frontend#targetted-browsers">}}), the chosen optimisation method is using 

`<link rel="preload" as="font" ... >` 

tags for `.woff2` format font files, and specifying the `font-display:` descriptor.

This causes browsers that support `.woff2` font files and the `preload` keywork on `link` elements (currently [89% of global web users](https://caniuse.com/?search=preload)) to initiate an early fetch for these font files that is separated from resource execution (ie doesn't depend on javascript or parsing CSS).

`font-display: swap` causes [supported browsers](https://caniuse.com/?search=font-display) (except IE 11) to render invisible text for 100ms (if the requested web font is not yet available), then the fallback font. If the requested web font becomes available at any time later, the text is re-rendered and replaced using the custom web font. This is suitable for situations where a user is unlikely to refresh the page for a considerable time, but the font is desirable and a reflow of content acceptable - for example, single page javascript applications.

`font-display: fallback` causes [supported browsers](https://caniuse.com/?search=font-display) (except IE 11) to render invisible text for 100ms (if the requested web font is not yet available), then the fallback font. If the requested web font becomes available within 3 seconds, the text is re-rendered and replaced using the custom web font. This is suitable for situations where a users is likely to refresh the page frequently, but a reflow of content would be undesirable - for example, a website or blog.

IE 11 doens't support the `font-display:` descriptor, but has default behaviour which is the same as `font-display: swap`.

Where possible, fallback fonts should be combined with suitable specific typographic settings within CSS to match the space taken by fallback font with the custom web font as closely as possible, using tools like [Font Style Matcher](https://meowni.ca/font-style-matcher/).

## Loading self-hosted fonts

Pages using self-hosted fonts should include 

`<link type="preload" as="font" href="..." type="font/woff2" crossorigin="anonymous" >` 

tags at the top of their `<head>` section for each font file required. Only the `.woff2` font file should be preloaded. For example, for Helvetica Neue regular:

```html
<link rel="preload" as="font" href="https://assets.crossref.org/private/fonts/903184/b8765d4b-d9a3-48b9-ac65-560e7517cf0e.woff2" type="font/woff2" crossorigin="anonymous">
```

Note the use of the `crossorigin="anonymous"` attribute - this is important, as browsers expect to load fonts files anonymously, and the preload request will be ignored without it.

The font should be added to the page's style using the `font-face` at-rule:

```css
@font-face {
  font-family:"Helvetica Neue";
  font-weight: 300;
  font-style: normal;
  font-display: fallback;
  src:
          local("Helvetica Neue"),
          url('https://assets.crossref.org/private/fonts/903184/b8765d4b-d9a3-48b9-ac65-560e7517cf0e.woff2') format('woff2'),
          url('https://assets.crossref.org/private/fonts/903184/d7d2e6c6-fc3a-41a5-9b52-648e12e215b2.woff') format('woff')
}
```

The `src` property instructs the browser to first look for a local (system) font called 'Helvetica Neue', thereafter to load the `.woff2` font (which should already be preloaded), and if it's Internet Explorer 11 or Safari 9, load the `.woff` font file from the CDN. 

## Futher optimisations of self-hosted fonts
A further optimisation is to create sub-sets of the font files, containing only the glyphs used in the application. This can be achieved through either physical subsetting of the font files, or using the `unicode-range` descriptor on a `font-face` declaration - which will cause the browser to only download a specific font file if a unicode character within the specified `unicode-range` is present on a page. This can be beneficial when supporting multiple language versions of pages (and creating the concomitant multi-language version of the font files).

## Font hosted by font platforms
Where possible, fonts hosted by third-party font platforms should be downloaded, converted to woff/woff2 if neccessary, and re-uploaded to the Crossref assets CDN. Thereafter, they should be treated as self-hosted font files.

If it's not possible to download and self-host the font files for any reason, then the [typekit/webfontloader](https://github.com/typekit/webfontloader) should be used. 
