---
related_services: []
repo_links: []
tags:
title: Custom form validation
weight: 0
---

Vuetify provides built-in validation of [form](https://vuetifyjs.com/en/components/forms/) inputs.


To use built-in validation, pass in an array of Javascript expressions to the `rules` prop of the form input.

For example: an array of `rules` in the wrapping component's `data` object:

```
  data () {
    return {
      rules: {
        required: value => !!value || 'Required.',
        minLength: value => (value && value.length >= 3) || 'Username must be 3 or more characters',
        email: value => {
          const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          return pattern.test(value) || 'Invalid email.'
        }
      }
    }
  }

```

Some of which are passed in as an array to the `rules` prop of the `<v-text-field>` [component](https://vuetifyjs.com/en/components/text-fields/)

```
  <v-text-field
    :rules="[rules.required, rules.minLength]"
    name="username"
    type="text"
  />
```

Rules are validated sequentially, and will display a maximum of one error at a time, so order matters.

If more complex validation is required, Vuetify can integrate [Vee-validate](https://logaretm.github.io/vee-validate/) or [Vuelidate](https://vuelidate.netlify.com/).

