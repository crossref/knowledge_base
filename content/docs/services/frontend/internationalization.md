---
related_services: []
repo_links: []
tags:
title: Internationalization of the Frontend Framework
weight: 0
---

The Frontend Framework is internationalized using the Vuetify translation function `t` and the built-in locale handling.

New locales can be added by creating a new file `locale.js` (eg `de.js`) in `src/locales`. Use an existing locale as a template to include the necessary imports.

Add the locale to `src/plugins/vuetify.js` and edit `src/components/LanguageMenu.vue` to include the new locale in that component's `locales` array.

The default locale is English, and the fallback for translation keys that don't exist (are not translated) in the current locale is English.

To use the translation service within a component, target the Vuetify lang service, eg:

```javascript
<v-btn>
    {{ $vuetify.lang.t('$vuetify.language_menu_button') }}
</v-btn>
```

To add translation strings, first add the new key and its English translation to the `src/locales/en.js` file. Then add the key to the locale files for which translations are available.