---
related_services: []
repo_links: []
tags: null
title: Sentry error logs
weight: 0
---

Unhandled errors are logged to the [frontend](https://sentry.io/organizations/crossref/issues/?project=5623269) Sentry project, via the Vue integration of the Sentry Javascript SDK. The SDK is initialised in the crossref-ui library bootstrap process, with the `release` set as `process.env.VUE_APP_NAME@process.env.VUE_APP_VERSION` and the environment as the vue app environment aka `process.env.NODE_ENV`.

## Redaction

Some user secrets are not picked up by the Sentry input filters on their backend - eg the value of the password input component (wrapping a Vuetify `v-text-field` it holds the password in a field named `value`), so there is a `beforeSend` handler which uses the `redact-object` helper to replace secret values with `[ REDACTED ]`.

## Releases

The SDK is configured to take the release as the name and version specified in `package.json`. These are made available in `vue.config.js`.

## Source Maps

The sentry webpack plugin is configured in `vue.config.js` to automatically build, validate and upload source maps to sentry during the build process. The authentication token is available from the CI variables. The `release` property matches that given to the SDK and is available in the CI environment as `SENTRY_RELEASE`.

The source mapping does not go all the way back to the original `.vue` component, as the frontend framework is built using the webpack 'library' target - this adds another layer of abstraction between the vue single file components and the source maps.
