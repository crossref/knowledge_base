---
area: tools-libraries
datadeps: []
desc: Tool for manually importing one-off data into Event Data.
docs: []
lang: Clojure
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://gitlab.com/crossref/tools/event_data_importer
sentry_url: ''
servicedeps:
- event-data-common
- event-data-event-bus
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- xml-transformation
title: Event Data Importer
userfacing: false
---



Tool for manually importing one-off data into Event Data.
