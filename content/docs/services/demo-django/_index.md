---
area: tools-libraries
datadeps: []
desc: Demo Django project with auto-generated API docs.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://github.com/CrossRef/demo-django-project
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Demo Django Project
userfacing: false
---



Demo Django project with auto-generated API docs.
