---
area: tools-libraries
datadeps: []
desc: Base image for all Event Data Docker images. Simply wraps some common dependencies
  pre-fetched into a Docker image to speed up building. Contains no code.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://github.com/CrossRef/event-data-docker-base
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Docker Base
userfacing: false
---



Base image for all Event Data Docker images. Simply wraps some common dependencies pre-fetched into a Docker image to speed up building. Contains no code.
