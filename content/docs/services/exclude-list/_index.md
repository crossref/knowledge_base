---
area: reporting-monitoring
datadeps: []
desc: ''
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls:
- https://data.crossref.org/excludeList
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
 - small-apps
title: Excluded Article Title List
userfacing: true
---

Reads MySQL:
 - titleDB

