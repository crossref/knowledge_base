---
area: greenfield
components:
- Meta
datadeps: []
desc: Console Frontend
docs: []
lang: Vue
legacy: false
packages: []
prod_heartbeats: []
prod_urls:
- https://crossref.gitlab.io/console
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/console
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Console Frontend
userfacing: true
---

The Frontend for [manage.crossref.org](https://manage.crossref.org), which houses our greenfield user interface. Built with [Vue.js](https://vuejs.org/), using the [Vuetify](https://vuetifyjs.com/en/) UI library.

## Targetted Browsers

We support browsers that implement the `es2022` JavaScript standard. See decision record [DR-0455: Browser version support](https://crossref.gitlab.io/engineering/decision-records/dr-0455/).

| Browser                         | Support Status |
|---------------------------------| -------------- |
| Chromium (Chrome, Edge Insider) | Supported |
| Edge                            | Supported |
| Firefox                         | Supported |     
| Safari 15+  	                   | Supported |
| Internet Explorer	              | Not Supported |

## Coding Style

Code should be [written in typescript]({{< relref "typescript.md" >}}) and should adhere to the [Prettier Style Guide](https://prettier.io/docs/en/why-prettier) with automatic formatting done by Prettier. The default ESLint config in the repo does this out of the box.

If existing Javascript code or libraries need to be used, they should at least have strongly typed interfaces.

Vue components should be built as [Single File Components](https://vuejs.org/v2/guide/single-file-components.html).

The frontend is compiled using Vite, with a build target of `es2022`. This means you can use new language features, and ones that aren't supported will be transpiled on build. 

## Linting

Code is linted with [ESLint](https://eslint.org/), using the .eslintrc config in the repo.

## Testing

Unit tests should be written for [Vitest](https://github.com/vitest-dev/vitest).

Intergration and component tests should be written with [Playwright](https://github.com/microsoft/playwright).

## Mocking endpoints

Under unit test, use Vitest's mocking functionality.

Under integration & component tests, use Playwright's mocking functions.

Under live local develpment, use Mock Service Worker.

## Environment-specific configuration

Environment config is parsed with `env.ts` which takes into account injected from the environment or `.env`.