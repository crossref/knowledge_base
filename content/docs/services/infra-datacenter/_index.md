---
area: infrastructure-services
datadeps: []
desc: The Data Center-based servers that we operate.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Data Center
userfacing: false
---



The Data Center-based servers that we operate.
