---
related_services: []
repo_links: []
tags: []
title: 'Architecture'
weight: 0
---

## Authentication setup in crossref environments

![cds-auth architecture](./auth-arch.png "Setup of CDS and authenticator in different environmants")

Community data is a service that relies on Authenticator. Ir order to communicate with authenticator they need shared secrets (`AUTH_SVC_USER` and `AUTH_SVC_PASSWORD`).

The same happens with CS and CDS, in order to communicate it is needed few secrets (`CS_CLIENT_SECRET` and `JWT_SIGNING_KEY`). In order to connect CS to CDS, those environment variables are needed plus the property `cs.auth.svc.baseUrl` that needs to point to the CDS base URL.

Those secrets are different for staging, sandbox and production environments.

When running CS locally, it can be connected to either one of the runnign environments (eg staging), or your own local CDS+Authentication deployment.

Running the authentication environment locally is strongly recommended, for that you will need to start the services using `docker-compose`:

`$ docker-compose -f docker-compose-auth.yml up` (use -d as well if you want to run it as a daemon)

Now the following services will be accessible:

* Authenticator: [http://localhost:8000/admin](http://localhost:8000/admin)
* Community-data: [http://localhost:9090/actuator/health](http://localhost:9090/actuator/health)

The first time you log into authenticator you can use *user*: `root` and *password*: `root`. Then you will be able to create more users and roles

Then you will have to add the following parameters to you personal properties file:

```
cs.auth.svc.baseUrl=http://localhost:9090
cs.auth.svc.client_secret=a55557848a884359a980dce828af33b8
cs.auth.svc.signing.key=5ff0617218c448d9acd6a3d1bc4c6f6c
cs.auth.root.pwd=root
```
