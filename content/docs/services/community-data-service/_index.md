---
area: infrastructure-apps
datadeps: []
desc: Represent our Membership and Community structures, who they are, and what they
  can do. Necessary for Access Control, billing, self-service and many other functions.
docs: []
lang: java
legacy: false
packages: []
prod_heartbeats: []
prod_urls:
- https://community-data.staging.crossref.org
- https://community-data.staging.dcbridgecrossref.org
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/community_data
sentry_url: ''
servicedeps:
- authenticator
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- authenticated
- auth
title: Community Data Service
userfacing: true
---

Represent our Membership and Community structures, who they are, and what they can do. Necessary for Access Control, billing, self-service and many other functions.

Current functionality:

 - Access Control for credentials
 - Issuance of tokens for internal use

To review the overall architecure of the new auth related-service, how they interact, and how they are envisioned to integrate with
the content system, refer to this [technical design document](https://docs.google.com/document/d/1JjZi2j5ppDdabKjSpBR2r1EESX1n0_ndFvofir23JtM/edit#).
