---
area: distribution-querying
datadeps: []
desc: ''
docs: []
lang: ''
legacy: true
packages:
- org.crossref.qs.controllers.GetResolvedReferences
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/getResolvedRefs/**
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- authorization-service
- citation-reference-finder
- ext-doi
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Get Resolved References
userfacing: false
---

The ability for members to find the metadata for content that cites their content. Members can make use of this service if they themselves deposit references. 

The format looks like this

https://doi.crossref.org/getResolvedRefs?doi=*DOI*&usr=*username*&pwd=*password*

And requires a username and password so that a member can only retrieve information for their own content.
