---
related_services: []
repo_links: []
tags: [deposit thread queue]
title: Deposit processor operations
weight: 0
---





## Development and JMX oeprations

### Instance properties definition 

Two new properties are available to the instance configurations:

`ds.depositprocessing.thread.config`
`ds.query.deposit.thread.config`

which are then used in the bean definitions for the `querySubmissionProcessorManager` or `dataSubmissionProcessorManager`
They take parameters defined with a double colon as a seperator (::) between fields. The query one has only two fields, as those were the only ones currently used for predefined threads, **maximum deposit** size and **prefered user**.


example:


```ds.depositprocessing.thread.config= \
    0::creftest \
    0::elsevier_new \
    0::elsevier_new \
    0::elsevier_new \
    0:: \
    0:: \
    0:: 
```

the trailing \ is already defined as a line continuation character.

### JMX operations

The previous operations are still available, but are no longer used by the deployment process to create threads. 
If threads need to be realigned, the `setThreadsToDefinition` operation is all that's needed. 
If an instance needs to be restarted, it will automatically restart its defined threads
If the defined threads need to be change while running, use the `setThreadDefinitions` option to define the threads as above, then run `setThreadsToDefinition` via JMX
**Note:** thread definitions change via JMX will not survive restart, only those stored in the properties file for the instance. 

### Technical details

There was previously (and is not removed) a parameter `ds.submission.dataprocessor.num-threads` for creating a number of generic threads. This is defined as 0, and has been previous to this new functionality. 

When the deposit processing instance starts up, the init method will call the `setThreadsToDefinition` which is also available as a jmx command, which reads the parameter definition defined above, and attempts to set the running threads to the defined ones. 

Thread creation and destruction now registers threads and so the above command will look through the running threads and determine which, if any, need to be killed or added to. 

Manually created threads (via the jmx/mbean) are not affected. 

There is another jmx command `setThreadDefinitions` to set the thread parameter definition after start.


The `addNewSubmissionProcessorThread` method reports to prometheus, and is called by any method of thread creation. 

Threads that are automatically created will have a name suffixed with `-autoCreated`

Thread execution finally will clear themselves from both the thread registry and prometheus registry.

The query and MD deposit processing threads are defined in this way, but the the unresolved and forward match processors are not. 
These each had a parameter `xs.unresolved-references-processing.factory.start-processors-count` and `xs.forward-matches-processing.factory.start-processors-count` respectively, that were implemented but were not working. They are working as of the changes with this ticket. The JMX operations for them have been left unchanged, but are no longer needed or used during deployment as the instances will now start the threads on their own. 
