---
area: reporting-monitoring
datadeps: []
desc: ''
docs: []
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Monitoring & Metrics System
userfacing: false
---



Content to follow
