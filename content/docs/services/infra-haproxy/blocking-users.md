---
related_services: []
repo_links: []
tags:
- blocking
title: Blocking Users
weight: 0
---

Users are blocked via version-controlled HAProxy config and data files. See the [README file on the HAProxy Repo](https://gitlab.com/crossref/haproxy-conf) for full details.

## View list of blocked IPs and user agents.

To view the current lists, look at:

 - [Blocked IPs file](https://gitlab.com/crossref/haproxy-conf/-/blob/master/cr5cd/blocked_ips.dat)
 - [Blocked User Agents file](https://gitlab.com/crossref/haproxy-conf/-/blob/master/cr5cd/blocked_useragents.dat)
 - [Blocked User Agents by regular expression](https://gitlab.com/crossref/haproxy-conf/-/blob/master/cr5cd/blocked_useragents_regex.dat)


More detail in the [README](https://gitlab.com/crossref/knowledge_base/-/blob/master/content/docs/services/infra-haproxy/blocking-users.md).

Additional information, like investigations and reasoning for blocking IPs or user agents, might be found by searching Slack and/or GitLab issues for the offending IP or user agent string.