---
area: infrastructure-services
datadeps: []
desc: Search Engine. Used by CS and REST API (deprecated).
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: SOLR
userfacing: false
---



Search Engine. Used by CS and REST API (deprecated).
