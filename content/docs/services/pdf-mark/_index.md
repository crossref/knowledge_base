---
area: tools-libraries
datadeps: []
desc: A tool and library offered to our members, without support, to help them prepare
  Crossmark-stamped PDFs. Embed DOI metadata within a PDF. Informal relationship with
  Crossmark.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://github.com/CrossRef/pdfmark
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: PDF Mark
userfacing: true
---



A tool and library offered to our members, without support, to help them prepare Crossmark-stamped PDFs. Embed DOI metadata within a PDF. Informal relationship with Crossmark.
