---
related_services: []
repo_links: []
tags: []
title: Assets CDN Design
weight: 0
---



## How it works

There is an AWS S3 bucket that stores the files. This is connected to a AWS CloudFront distribution that performs the CDN. When a file is changed the distribution should be invalidated to flush the cache.

## Automation

GitLab CI automation is used to deploy to S3 from the master branch. The project's CI settings should be configured with the following environment variables:


 - `DIST_ID` - the CloudFront distribution ID
 - `S3_BUCKET` - the S3 bucket where data is stored
 - `AWS_ACCESS_KEY_ID` - AWS access key with permission to upload to S3 and invalidate CloudFront.
 - `AWS_SECRET_ACCESS_KEY` - AWS secret key.

The IAM user should have the following permissions, no more.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "cloudfront:ListCloudFrontOriginAccessIdentities",
                "s3:PutAccountPublicAccessBlock",
                "s3:GetAccountPublicAccessBlock",
                "s3:ListAllMyBuckets",
                "s3:ListJobs",
                "cloudfront:ListInvalidations",
                "cloudfront:ListDistributions",
                "s3:CreateJob",
                "cloudfront:ListStreamingDistributions",
                "s3:HeadBucket",
                "cloudfront:CreateInvalidation",
                "cloudfront:ListDistributionsByWebACLId"
            ],
            "Resource": "*"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": [
                "arn:aws:s3:::assets.crossref.org",
                "arn:aws:s3:::assets.crossref.org/*",
                "arn:aws:s3:*:*:job/*"
            ]
        }
    ]
}
```