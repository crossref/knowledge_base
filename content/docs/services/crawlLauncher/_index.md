---
area: distribution-querying
datadeps: []
desc: 
docs:
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products:
- reports
related_services: []
repo_links:
- https://gitlab.com/crossref/crawl_launcher
sentry_url: ''
servicedeps:
- infra-datacenter-mysql
- infra-datacenter-oracle
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: 
 - small-apps
title: Crawl Launcher
userfacing: true
---

Cron (Every 5 minutes), launches newDoiCrawler when needed

Reads MySQL:
 - titleDb
 - crossref

Writes MySQL:
 - titleDB