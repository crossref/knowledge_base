---
area: documentation
datadeps: []
desc: Discource-powered community forum
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Crossref Community Forum
userfacing: true
---




The Crossref Community forum. This is hosted externally on the commercial Discourse servers.

