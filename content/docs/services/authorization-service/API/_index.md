---
area: infrastructure-apps
datadeps: []
desc: API endpoint for authentication and authorisation
docs: []
lang: ''
legacy: true
packages:
- org.crossref.ds.security.AuthorizationService
- org.crossref.qs.security.AuthorizationService
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/servlet/login
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Authentication API
userfacing: false
---

The authentication API is an endpoint that allows the user to authenticate and authorise for a given role.

CS is the public facing service that provides authentication for users and it has implemented an internal caching system to reduce load on the community_data service that has a TTL of 30 seconds. Be aware of the delay this caching system migh have when modifying users permissions and configurations.

All requests that contain login information will return a HTTP 200 return code in case of successful authentication (correct password) and authorisation (specified role). Otherwise a 401 return code will be returned.

In case a role is not chosen, by default a role matching the username will be used. If it exists a role with such name, authorisation will automatically be granted without specifying a role.

In the case the user has only one role associated with it and it is different than the username, this role will have to be specified in the request in order to get a successful authorisation.

The parameters for the query are:

| Parameter | Description                                                  |
| --------- | ------------------------------------------------------------ |
| usr       | Username to authenticate, it can be a legacy credential user or an email address |
| pwd       | Password                                                     |
| role      | role to use                                                  |



The response of the API endpoint is a JSON object that might contain:

 

| key             | value                                                     |
| --------------- | --------------------------------------------------------- |
| `roles`         | list of associated roles with the user                    |
| `errorMessage`  | Message in case of failed authentication                  |
| `message`       | Message if authentication was successful                  |
| `authenticated` | `true` if authentication was successful `false` otherwise |
| `authorised`    | `true` if authorisation was successful `false` otherwise  |
| `Authorization` | Access token to access services                           |
| `refresh_token` | refresh token to renew acess token                        |
| `identity`      | identity assigned to the user                             |
|                 |                                                           |



Examples:

Url: `https://doi.crossref.org/servlet/login?usr=email@organisation.com&pwd=mypassword`

Response:

```
{
  "roles" : [
    "role1",
    "role2",
    "role3"
  ],
  "errorMessage" : "User 'email@organisation.com' cannot assume specified role 'email@organisation.com'",
  "success" : false,
  "redirect" : "/servlet/login",
  "authenticated" : true,
  "authorised" : false
}
```

If the parameter `role` is not provided the system will try to authorise the user with a role that maches the username. In case such role does not exist the user will be authenticated but not authorised and therefore credential tokens will not be generated. A `roles` key will be provided containing a list of valid roles associated to the authenticated username.

Url: `https://doi.crossref.org/servlet/login?usr=email@organisation.com&pwd=mypassword&role=role2`

```
{
  "success" : true,
  "message" : "Authenticated",
  "redirect" : "/servlet/home",
  "roles" : [
    "role1",
    "role2",
    "role3"
  ],
  "Authorization" : "Bearer XXXXXXXXX.XXXXXXXX.XXXXX",
  "refresh_token" : "YYYYYYYYY.YYYYYYYYYYY.YYYYYY",
  "identity" : "email@organisation.com:role2",
  "authenticated" : true,
  "authorised" : true
}
```

If the authentication is accepted and a valid role is provided the response JSON object will contain JTW tokens for authorising and for refreshing the access token.
