---
area: infrastructure-apps
datadeps: []
desc: The AuthorizationService determines the permissions for the actor with a given
  set of identities
docs: []
lang: ''
legacy: true
packages:
- org.crossref.ds.security.AuthorizationService
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Authorization Service
userfacing: false
---




Content to follow.

