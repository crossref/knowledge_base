---
area: etl
datadeps: []
desc: ''
docs: []
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products:
- reference-linking
- cited-by
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- matching
title: Forward Match System
userfacing: false
---

Useful documentation: https://docs.google.com/document/d/1srx7Xdl-EMHqP3FzBOXhXyvNwBok96L5CBgiHAlsAhE/edit?tab=t.0#heading=h.nuzmtnx10tbr

Content to follow
