---
area: tools-libraries
datadeps: []
desc: ''
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls:
- https://data.crossref.org/reports/schematron
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Schematron
userfacing: true
---

Served from `ftp-reports` backend.
