---
related_services: []
repo_links: []
tags:
- elasticsearch
- authentication
title: Elasticsearch backend in REST API
weight: 0
---

## Authentication

When using Elasticsearch in AWS, all requests should be signed with AWS credentials. Cayenne signs requests to ES by default. This can be disabled by passing env var `ELASTICSEARCH_NO_AUTH=1` or in the code by setting `[:service :elastic :no-auth]` configuration parameter to `true`.

The code for creating an Elasticsearch client that will sign requests can be found in `cayenne.elastic.util/es-client` function. The code is based on an official [Java example](https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/es-request-signing.html#es-request-signing-java).