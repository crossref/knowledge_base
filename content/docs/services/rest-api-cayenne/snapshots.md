---
related_services: []
repo_links: []
tags:
- snapshots
- json
title: Serving snapshots in REST API
weight: 0
---

After migration to ES REST API in AWS, snapshots will be stored in a new snapshot bucket and served directly by REST API.

All pools allow for listing snapshot files in the bucket through `/snapshots` route (staging: https://api.staging.crossref.org/snapshots). No authentication is required to list the files. Only files with S3 prefix `monthly` are listed; failed snapshots and snapshot statistics are also stored in the bucket but not listed.

Only PLUS pool allows to download snapshot files. This is controlled by env var `SNAPSHOT_DOWNLOAD`, set to `1` for PLUS API only. PLUS tokens are verified by haproxy.

When asked for a snapshot file, PLUS API generates a redirect with a pre-signed S3 URL. This URL expires in 10 minutes by default. The time can be configured by env var `SNAPSHOT_BUCKET_FILE_EXPIRY_MINS`.
