---
datadeps:
- metadata-bucket
related_services:
- metadata-bucket-updates
- metadata-bucket
repo_links: []
tags:
- metadata
- sqs
- s3
title: Ingesting metadata from AWS SQS and S3
weight: 0
---

## API ingesting functionality

### Ingesting directly from S3

Startup tasks `:s3-ingest-xml` and `:s3-ingest-update` will cause the REST API to ingest metadata directly from S3 Metadata Bucket. When run with `:s3-ingest-xml` or `:s3-ingest-update`, the rest API will page through the data (XMLs or cited-by counts, respectively) in Metadata Bucket and index the data in Elasticsearch.

Instead of scanning the entire Metadata Bucket, it is possible to index a list of works, either by passing a comma-separated list of DOI in `UPDATE_DOIS` env var, or by passing a comma-separated list of S3 prefixes in `UPDATE_S3_PREFIXES` env var.

If you do not wish to rely on S3 then you can use a local directory, passing `METADATA_LOCAL_STORAGE=1`.

**Note** Metadata Bucket must have been built using [Metadata Bucket Builder](../../metadata-bucket-updates/#metadata-bucket-builder) or another tool that conforms to [the spec](../../metadata-bucket/).

**Note** This ingesting is done in a single thread and is not scalable at the moment. It is used mostly for local development/debugging, not in production.

### Ingesting from SQS and S3

When run with `:sqs-ingest` startup task, the REST API will consume messages from SQS. This task continuously polls the SQS, receiving and handling messages. In our staging/production deployment, `:sqs-ingest` is used by the indexers.

Currently, there are three types of messages that REST API processes:
- a message containing S3 key of an XML file: such a message is produced to the queue whenever a new XML file is added to, or existing XML file is replaced in the Metadata Bucket, it causes the indexer to read the XML file from the Metadata Bucket, parse it and index it in ES
- a message containing S3 key of an cited-by count (CBC) JSON file: such a message is produced to the queue whenever a new CBC JSON file is added to, or existing CBC JSON file is replaced in the Metadata Bucket, it causes the indexer to read the CBC JSON file from the Metadata Bucket, parse it and update the cited-by count of a DOI in ES
- a message containing S3 key of an XML file and `index-all` flag: such a message is produced to the queue when `:s3-sqs-produce-all` startup task is used to [push messages to SQS](#pushing-to-sqs), it causes the indexer to read all files for a given DOI (currently: XML and CBC JSON) from the Metadata Bucket and index both in the right order in ES (currently: first XML is indexed, then cited-by count is updated)

### Pushing to SQS

When run with `:s3-sqs-produce-xml`, `:s3-sqs-produce-update` or `:s3-sqs-produce-all` startup tasks, the rest API will page through all the keys from the Metadata Bucket and push relevant messages to SQS. These messages can then be consumed by [ingesting from SQS](#ingesting-from-sqs-and-s3).

There are three tasks for pushing messages to SQS:
- `:s3-sqs-produce-xml` scans the Metadata Bucket and for every XML file pushes a message containing S3 key of this file 
- `:s3-sqs-produce-update` scans the Metadata Bucket and for every CBC file pushes a message containing S3 key of this file 
- `:s3-sqs-produce-all` scans the Metadata Bucket and for every XML file pushes a message containing S3 key of this file and `index-all` flag, telling the indexer to index the XML file and all additional files for this DOI 

Instead of scanning the entire Metadata Bucket, it is possible to push a list of works, either by passing a comma-separated list of DOI in `UPDATE_DOIS` env var, or by passing a comma-separated list of S3 prefixes in `UPDATE_S3_PREFIXES` env var.

## Indexing procedures

### Building the full index and populating the Metadata Bucket at the same time

Once per environment, at the very beginning, we need to build the work indexes for the first time and also populate the Metadata Bucket. The procedure is as follows:

1. Start pushing periodic updates of XMLs into the bucket. This is done first so that we do not miss any update while pushing the entire corpus. REST API's indexers start receiving messages from SQS and start indexing new DOIs and updates of DOIs.
2. Use command-line pusher to push all the XMLs to the bucket. Wait until all that is indexed.
3. Examine and address any errors appearing in Sentry during the ingestion, known are:
- ParsingException java.lang.Exception: Null DOI in parsed XML - these are exceptions occuring during XML parsing, usually unsupported content type (eg. pending publications, conference series), DOIs with XML metadata missing, DOIs with empty suffix, DOIs with a space, DOIs from other registration agencies. These cannot be fixed by reindexing, so file an issue if one does not exists and otherwise ignore.
- Exception Missing crm-item X for DOI - this means some crm-item, usually member id or publisher name, is missing from the XML in the bucket. These DOIs are indexed, but they will have null metadata in the REST API output. To fix, we need to re-push correct XML files to the bucket. The list can be grepped from the logs.
- AmazonS3Exception We encountered an internal error - this means the indexer was unable to read the files from the Metadata Bucket. This can be fixed by pushing files through `:s3-sqs-produce-xml` startup task, which is done by the scanner in our deployment. The list can be grepped from the logs.
4. Start pushing periodic updates of CBC JSONs into the bucket.
5. Use command-line pusher to push all CBC JSONs to the bucket. Wait until all that is indexed.
6. Examine and address any errors appearing in Sentry during the ingestion and fix accordingly.

{{< mermaid >}}
graph LR;
  pusher(Pusher)
  s3(S3 Bucket)
  sqs(AWS SQS queue)
  indexer(REST API Indexer)
  es(Elasticsearch)
  pusher--XMLs & CBCs-->s3
  s3--Messages-->sqs;
  sqs--Messages-->indexer
  s3--XMLs & CBCs-->indexer
  indexer--ES JSON-->es
{{< /mermaid >}}

### Building the full index from the Metadata Bucket

Sometimes there is a need to rebuild the index from the Metadata Bucket, without re-pushing everything.

If there are no changes to Cayenne mappings, or the only changes are new fields being added, the procedure is as follows:

1. Update mappings in Cayenne’s source code, if a change is needed. Redeployment will restart the containers and modify the mappings in place.
2. Start Cayenne scanner (1 instance per pool) with startup task `:s3-sqs-produce-all`. This will scan the S3 metadata bucket and push a message about every XML file and cited-by update into the queue. Indexers will then receive those messages and index the files. Wait until this ends.
3. Stop Cayenne scanner.

If a change in Cayenne mapping is needed, the procedure involves creating a new field with the new mapping and switching the code to the new field:

1. Add the new field with the new mapping to mappings in Cayenne's source code. The name should be significant, eg. `x-text` if the type will be text, or something similar. Redeployment will restart the containers and add the new field in ES.
2. Modify the indexing code to index the data in the new field alongside the old field. From now on, the data will be indexed in the both old and new fields.
3. Start Cayenne scanner (1 instance per pool) with startup task `:s3-sqs-produce-all`. This will scan the S3 metadata bucket and push a message about every XML file and cited-by update into the queue. Indexers will then receive those messages and index the files. Wait until this ends.
4. Stop Cayenne scanner.
5. Switch the reading code to the new field.
6. Remove indexing to the old field. The data will remain in the index, but no code will be using it.

{{< mermaid >}}
graph LR;
  indexer(REST API Indexer)
  scanner(REST API Scanner)
  s3(S3 Bucket)
  sqs(SQS)
  s3--keys-->scanner
  s3--XMLs & CBC-->indexer
  scanner--messages-->sqs
  sqs--messages-->indexer
  indexer--ES JSON-->es
{{< /mermaid >}}

