---
related_services: []
repo_links: []
tags:
- xml-transformation
- xml
- json
title: Crossmark metadata in REST API
weight: 0
---

Crossmark metadata is returned as part of work metadata in `update-to` field. The field contains the following subfields:
* `updated`, ingested from XML metadata
* `DOI`, ingested from XML metadata
* `type`, ingested from XML metadata
* `label`, its value is one of the following:
  * taken from a controlled vocabulary based on `type`, if the type exists in the vocabulary (the vocabulary is stored in `cayenne.ids.update-type` namespace in the code)
  * ingested from XML metadata, if the type does not exist in the vocabulary and label is present in XML
  * equal to `type`, if the type does not exist in the vocabulary and label is not present in XML