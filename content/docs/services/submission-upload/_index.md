---
area: tools-libraries
datadeps: []
desc: A command line tool for uploading submission files to Crossref's deposit system.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system_uploader
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Submission Upload Tool
userfacing: true
---

A command line tool for uploading submission files to Crossref's deposit system.
