---
area: infrastructure-services
datadeps: []
desc: AWS Simple Storage Service
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: S3
userfacing: false
---

AWS Simple Storage Service (S3) is an object storage service. It is used for the following purposes:

 - Storing first-class data objects, e.g. Events
 - Unbounded general-purpose key-value store, e.g Percolator Checkpoints
 - Indexing data obejcts, e.g. Events by prefix
 - Serving websites via CloudFront, e.g. the current Event Data User Guide.
