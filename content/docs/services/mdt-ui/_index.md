---
area: ingestion
datadeps:
- mdt-middleware
desc: Metadata deposit tool user interface. Note that the middleware has further dependenciesxx.
docs: []
lang: JavaScript
legacy: true
packages: []
prod_heartbeats:
- https://www.crossref.org/mmstaging
prod_urls:
- https://www.crossref.org/metadatamanager
products:
- metadata-manager
related_services: []
repo_links:
- https://github.com/CrossRef/mdt-ui
sentry_url: ''
servicedeps:
- mdt-middleware
- infra-datacenter-apache
- assets-cdn
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Metadata Manager UI
userfacing: true
---



Metadata deposit tool user interface. Note that the middleware has further dependenciesxx.
