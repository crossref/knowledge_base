---
area: integrity
datadeps: []
desc: The download of the Retraction Watch data
docs: []
lang: Python
legacy: true
packages: []
prod_heartbeats: []
prod_urls:
- https://api.labs.crossref.org/data/retractionwatch
products:
- 'retraction watch'
related_services: []
repo_links:
- https://gitlab.com/crossref/labs/retraction-watch-import
- https://gitlab.com/crossref/labs/lambda-api-proxy
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: 'Retraction Watch'
userfacing: true
---


The Retraction Watch database was purchased by Crossref in 2023 and provides a human-verified list of retracted journal articles.

The system is updated on a daily basis by the [[Airflow import script]](https://gitlab.com/crossref/labs/retraction-watch-import), running on the "Research" AWS account (under "Managed Airflow"). This script updates annotations for any works that have retractions attached.

## Serving the Retraction Data
There are two ways that we serve the retraction data:

1. Through the Labs API. Example: https://api.labs.crossref.org/works/10.3758/s13415-014-0326-3?mailto=labs@crossref.org
2. By downloading the whole CSV from the Labs API. URL: https://api.labs.crossref.org/data/retractionwatch

The former is created by using the importer Airflow script to update annotations for every single DOI that has a retraction entry associated with it. The latter is just a fall-through proxy to give the users the daily CSV.

## Updates from Retraction Watch
Retraction Watch send us a new .CSV file every working day of the week. This file appears to have UTF-8 errors, but can be opened in latin-1 mode with some bumps in Russian author names, or in UTF-8 mode in Python with errors ignored.

## New Database Software
The process for Retraction Watch to update their database and export to us is onerous. The software is closed source and the process is somewhat painful, involving an initial export to Excel format followed by a hop to CSV. A prototype new interface has been created and is hosted at https://gitlab.com/crossref/labs/retraction-watch-database. However, Retraction Watch have not had time to run through parallel testing of this new system and seem keen to stick to their existing challenge.

## Transfer to Production
Martyn Rittman, Patricia Feeney, and Martin Eve have been in discussions around moving these data to production. This includes REST API schema design but has not yet looked at the periodic import.