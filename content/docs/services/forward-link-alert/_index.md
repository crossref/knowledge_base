---
area: distribution-querying
datadeps: []
desc: Alert members about forward link matches.
docs: []
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products:
- reference-linking
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- matching
title: Forward Link Alert System
userfacing: true
---

Alert members about forward link matches (forward link is synonymous with cited-by).

Alerts are initiated by a metadata query containing the email to send alerts to, the doi target and an flink tag:
```
<?xml version = "1.0" encoding="UTF-8"?>
<query_batch version="2.0" xmlns = "http://www.crossref.org/qschema/2.0"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.crossref.org/qschema/2.0 http://www.crossref.org/qschema/crossref_query_input2.0.xsd">
<head>
	<email_address>your@email.com</email_address>
 	<doi_batch_id>fl_001</doi_batch_id>
</head>
<body>
	<fl_query alert="true">
  	<doi>10.1053/sonc.2002.35642</doi>
	</fl_query>
</body>
</query_batch>
```
Once emails are registered, to remove them you must delete the entries from the flink_alerts table in oracle (mysql deposit in test).
