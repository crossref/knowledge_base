---
area: infrastructure-services
datadeps:
- deposit-processor
- deposit-queue
desc: Queue and Notification system.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- infra-datacenter
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: ActiveMQ (DataCenter)
userfacing: false
---



Queue and Notification system.
