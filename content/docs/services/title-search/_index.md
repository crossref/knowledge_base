---
area: distribution-querying
datadeps: []
desc: ''
docs: []
lang: ''
legacy: true
packages:
- org.crossref.qs.controllers.TitleSearchController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/title/**
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- authorization-service
- title-db
- members-service
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Title Search
userfacing: false
---


Content to follow