---
area: reporting-monitoring
datadeps: []
desc: ''
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls:
- https://data.crossref.org/ignoreFields
products: []
related_services: []
repo_links:
 - https://gitlab.com/crossref/ignore_fields
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
 - small-apps
title: Field Report - Ignore Field(s) Request
userfacing: false
---

Reads MySQL:
 - titleDB

Writes MySQL
 - titleDB

