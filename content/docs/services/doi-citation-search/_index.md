---
area: distribution-querying
datadeps:
- citation-search-service
desc: ''
docs: []
lang: ''
legacy: true
packages:
- org.crossref.qs.controllers.DOICitationSearchController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/search/doi/**
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- citation-search-service
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: DOI Citation Search
userfacing: false
---




Content to follow.

