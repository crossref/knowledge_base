---
area: infrastructure-services
datadeps: []
desc: Oracle database running in data center.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- infra-datacenter
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Oracle Datacenter
userfacing: false
---



Oracle database running in data center.
