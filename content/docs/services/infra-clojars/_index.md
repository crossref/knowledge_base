---
area: infrastructure-services
datadeps: []
desc: Storage of Clojure library artifacts.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls:
- https://www.clojars.org
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Clojars Repository
userfacing: false
---



Storage of Clojure library artifacts.
