---
area: distribution-querying
datadeps:
desc: 
docs:
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products:
- reports
related_services: []
repo_links:
- https://gitlab.com/crossref/customer_db_reports
sentry_url: ''
servicedeps:
- infra-datacenter-mysql
- infra-datacenter-oracle
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
 - smallapps
title: Customer DB Reports
userfacing: true
---

Web app. Uses single DB connection for all DBs

Reads from Oracle / PostgreSQL.

Reads from MySQL:
 - titleDB
 - resReport
 - crossref
 - ftqLog

