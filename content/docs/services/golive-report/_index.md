---
area: reporting-monitoring
datadeps:
- ext-doi
desc: Generate golive and stats reports and push content to website.
docs: []
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls:
- https://data.crossref.org/reports/statusReport.html
- https://data.crossref.org/reports/50go-live.html
products:
- metadata-retrieval
related_services: []
repo_links:
- https://https://gitlab.com/crossref/golive-status-report
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: GoLive Status report
userfacing: true
---

Cron (2 AM every day), generates status.html and golive.html and uploads them to FTP

Generate golive and stats reports and push content to website.

Reads PostgreSQL / Oracle.

Reads MySQL: 
 - titleDB

Uses FTP / Filesystem

