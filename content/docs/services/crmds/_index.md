---
area: distribution-querying
datadeps:
- rest-api-cayenne
desc: Crossref search interface
docs: []
lang: Python
legacy: false
packages: []
prod_heartbeats:
- https://search.crossref.org/heartbeat
prod_urls:
- https://search.crossref.org
products:
- metadata-retrieval
- metadata-search
related_services: []
repo_links:
- https://gitlab.com/crossref/search
sentry_url: ''
servicedeps:
- rest-api-cayenne
sonar_url: ''
staging_heartbeats: []
staging_urls:
- https://search.staging.crossref.org
tags: []
title: Crossref Metadata Search & Funder Search
userfacing: false
---


Crossref search interface


This has been rewritten. It was previously in Ruby: https://gitlab.com/crossref/metadata_search 
