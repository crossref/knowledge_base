---
related_services: []
repo_links: []
tags:
- orcid
title: ORCID data and storage in Crossref Metadata Search
weight: 0
---

The Crossref Metadata Search (CRMDS) app is configured with ORCID Client ID, Client Secret and Redirect URL.

This uses the following ORCID OAuth scopes:

 - `/read-limited`
 - `/activities/update`

# Storage of ORCID user data

When the user logs in or imports their works, we retrieve their Given Name and Family Name from the `/person` API and. It is stored in a server-side session. This is so that it can be displayed to the user in the user interface.

The CRMDS server-side session expires at the `expires_at` specified by the ORCID OAuth2 provider response.
 
When an ORCID claim is made, the CRMDS server POSTs a work to ORCID's `/work` endpoint using the user's OAuth token. From there, the association is stored in the ORCID system. 

The CRMDS server stores a list of ORCIDs of users with DOI claims, along with DOIs that have been claimed against them. It is populated / updated whenever the user uses the CRMDS Sync function (which they are prompted to do on login), or when they claim or unclaim an orcid.

If a user wishes to remove a claim from their record, they must do it from within ORCID's own user interface. They can then perform a sync in CRMDS, which will update this status in the server-side storage. The CRMDS interface will then display 'NOT VISIBLE' next to that work in the search results, indicating that a claim has been stored on the server, but that doesn't match what ORCID represents. Clicking this will allow a user to click 'remove' to remove it completely from the CRMDS database.

# Display of ORCID user data

CRMDS stores ORCID information from two routes:

 - From the Publisher Metadata, visible to all users
 - From the claim (detailed above), visible only to logged in user who made the claim.
 
Users may request that we remove their claims from CRMDS with a support request. However, this data is visible only to them and never displayed to the public.

