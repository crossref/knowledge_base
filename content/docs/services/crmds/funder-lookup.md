---
related_services: []
repo_links: []
tags:
- CRMDS Funder Works Lookup
title: Associated works for selected funder lookup in CRMDS
weight: 0
---

The Funder lookup in the Crossref Metadata Search service uses the javascript library: `typeahead.js`. The library has two 