---
related_services: []
repo_links: []
tags: []
title: Event Data Announcements
weight: 0
---




An Announcement is a file, made available at a stable URL, that describes a significant event, for example an outage or data maintenance operation. It is encoded as JSON, and includes a timestamp, title and message. A new announcement should be created every time we make a change to an Event, and the Event should link to the announcement. This allows every Event that has been manually updated to describe why it was updated.

For example [this announcement](https://evidence.eventdata.crossref.org/announcements/2018-04-26T00-00-00Z-ED-16.json) describes that we found a bug, and that we updated Events to correct problem data:

    {
      "id": "2018-04-26T00-00-00Z-ED-16",
      "date": "2018-04-26T12:00",
      "tags": [],
      "type": "updates",
      "title": "Tidy up Evidence Record URLs",
      "message": "The evidence_record field should be a complete URL. Some events were created with partial URLs. These are being fixed."
    }


And [https://evidence.eventdata.crossref.org/announcements/2017-10-06T00-00-00Z-ED-12.json](this one) describes updates we made to Events when we introduced Canonical URLs:

    {
      "id": "2017-10-06T00-00-00-00Z-ED-12",
      "date": "2017-10-06T16:00",
      "tags": [
        "newsfeed",
        "web",
        "reddit-links",
        "wordpressdotcom"
      ],
      "type": "updates",
      "title": "Canonical URLs",
      "message": "A new feature in the Percolator looks for the canonical URL of webages it visits. When it finds one, it uses it as the subj_id and subj.pid. The new feature applies to newly collected Events. All previously collected Events are being updated to bring them in line."
    }

Finally, [this Announcement](https://evidence.eventdata.crossref.org/announcements/2017-05-08T08-41-00Z-CED-9.json) describes the process of Twitter compliance checking, and every time a Twitter Event is updated a link is placed in the Event to point here:

    {
      "id": "2017-05-08T08-41-00Z-CED-9",
      "date": "2017-05-08T08:41",
      "tags": [
        "twitter"
      ],
      "type": "compliance",
      "title": "Ongoing Twitter Compliance",
      "message": "We monitor Twitter for deletions of Tweets. When we detect that an Event refers to a Tweet that was has been deleted, we will edit the Event to remove the relevant data. See the User Guide for full information."
    }

  
## FAQ

### Why implement something separate to the status page? 

Announcements are hosted on our own Event Data infrastructure, alongside Evidence Records. Their URLs are considered stable identifiers that we can embed links to in Events. As they are encoded as JSON they are not necessarily designed to be user-facing. The Crossref status page, which was created more recently, could link to the Announcements in situation where that would be pertinent.
