---
area: distribution-querying
datadeps:
- members-service
desc: ''
docs: []
lang: ''
legacy: true
packages:
- org.crossref.qs.view.UnixsdView
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- members-service
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- crm-items
title: UNIXSD View
userfacing: true
---

The UNIXSD view formats XML. It adds the schema locations:

 - `http://doi.crossref.org/schemas/unixref1.1.xsd`
 - `http://doi.crossref.org/schemas/unixref1.0.xsd`
 
It adds adds CRM Items:

  - `publisher-name`
  - `prefix-name`
  - `member-id`
  - `citation-id`
  - `journal-id`
  - `book-id`
  - `series-id`
  - `deposit-timestamp`
  - `owner-prefix`
  - `prime-doi`
  - `last-update`
  - `created`
  - `citedby-count`


See [XML-Updates Topic Page]({{< ref "/docs/topics/xml-updates-transformations" >}}) for a discussion on CRM-items, as they are also added by the DOI Info View.
