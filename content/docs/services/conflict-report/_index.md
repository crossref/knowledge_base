---
area: reporting-monitoring
datadeps:
- ext-doi
desc: Generate reports and send links to members by email.
docs: []
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls:
- https://data.crossref.org/reports/conflict.html
products:
- metadata-retrieval
related_services: []
repo_links:
- https://gitlab.com/crossref/conflict_report
sentry_url: ''
servicedeps:
- infra-datacenter-mysql
- infra-datacenter-oracle
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Conflict report
userfacing: true
---

Generate reports and send links to members by email. Cron (11 AM every Tuesday), sends emails.

Reads Oracle / PostgreSQL. 

Reads MySQL:
 - titleDb
 - crossref

Writes MySQL:
 - titleDB

Uses FTP / filesystem, email, SugarCRM