---
area: legacy
datadeps: []
desc: Tools for performing manual tasks within the Content System
docs: []
lang: ''
legacy: true
packages:
- org.crossref.projects
prod_heartbeats: []
prod_urls: []
products: []
related_services:
- deposit-processor
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Content System Tools
userfacing: false
---

Tools for performing manual tasks within the Content System.