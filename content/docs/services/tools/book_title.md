---
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
tags:
- book
- book-title
- tool
title: Bulk Title Update Tool
weight: 0
---

This tool allows the processing of CSV files for bulk updating of book titles in the system.

The CSV file must follow these rules:

- The first line must be a heading line. 
- The heading line must contain a `new_title` column.
- The other headings may be:
   - `pisbn`
   - `eisbn`
   - `old_title`
   - `bookciteid`
   - `doi`
  At least *one* identifier must be included (`bookcite`, `doi`, or some isbn).
- If multiple identifiers are included, the code will prefer `bookciteid`, then `doi`, then isbns.
- Column order does not matter.
- The code deals with misspelled title updates and main book title updating.

This can be run from a local directory by pulling a war file, deploying it and then running using the included script (bulkTitleUpdateTool.sh) and deployment property file (deployment-inf3-bulkTitleUpdateTool.properties).

