---
area: distribution-querying
datadeps: []
desc: 
docs:
lang: Java
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products:
- reports
related_services: []
repo_links:
- https://gitlab.com/crossref/doi_error_catcher
sentry_url: ''
servicedeps:
- infra-datacenter-mysql
- infra-datacenter-oracle
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
 - small-apps
title: DOI Error Catcher
userfacing: true
---

Reads MySQL:
 - titleDB

WRites MySQL:
 - titleDB


The DOI error catcher is a servlet that receives the CNRI errors and logs them in a MySQL db.
