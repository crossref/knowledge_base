---
area: registries
datadeps:
- human-direct-edit
desc: Operational Rules for automated decisions. Open. Currently only for ORCID claims
  block list.
docs: []
lang: CSV
legacy: true
packages: []
prod_heartbeats: []
prod_urls:
- https://gitlab.com/crossref/rules/raw/master/blocked-orcid-claims.csv
products:
- orcid-auto-update
related_services: []
repo_links:
- https://gitlab.com/crossref/rules
sentry_url: ''
servicedeps:
- infra-gitlab
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- orcid
title: Operational Rules
userfacing: false
---



Operational Rules for automated decisions. Open. Currently only for ORCID claims block list.
