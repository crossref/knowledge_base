---
area: infrastructure-services
datadeps:
- event-data-evidence-log
desc: Storage and querying of logs for services running within AWS.
docs: []
lang: ''
legacy: true
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- infra-aws
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: AWS Cloudwatch
userfacing: false
---



Storage and querying of logs for services running within AWS.
