---
related_services: []
repo_links: []
tags: []
title: Crossref Knowledge Base
weight: 0
---

Welcome to the internal Crossref Knowledge Base! This site is <strong>internal documentation</strong> for use by staff. It's a view of the well-established and legacy subsystems. 

Browse the list of services in the menu. The granuality of decomposition of our system into services is meant to aid understanding. Each one is either a microservice, Controller, or subsystem as appropriate. 


For future-looking documentation see the [Crossref Engineering Site](https://crossref.gitlab.io/engineering/). 

If you don't work at Crossref, you probably want the [Crossref Documentation](https://www.crossref.org/documentation/) site.


## Track down code
 - [List of Service URLs]({{< relref "/prod_urls" >}})

## Bigger picture
 - [List of Topic Tags]({{< relref "/tags" >}})
 - [List of Areas]({{< relref "/area" >}})
 
 ## Diagrams
 - This [Dependency Diagram](/knowledge_base/rendered/dependencies.pdf) ([SVG version](/knowledge_base/rendered/dependencies.svg)) is built from the data on this site. Each box is a service. 

