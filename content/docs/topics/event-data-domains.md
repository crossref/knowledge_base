---
desc: ''
products: []
related_services: []
repo_links: []
tags:
- web-domains
- event-data
title: Web Domains monitored by Event Data
weight: 0
---

## What it means to 'monitor' a domain

Event Data Agents monitor different parts of the web in different ways. 

Agents may:

 - be written specifically for a particular website, e.g. Reddit or Twitter
 - monitor a given source, which includes links off to other domains, e.g. Reddit Links.
 - monitor a list of feeds, each of which may link off to websites on different domains, e.g. an RSS feed that aggregates other RSS feeds.
 - gather data from sources not directly controleld by Crossref, e.g. commmunity agents
 
For these reasons, we cannot know in advance the set of domains where we may find links. Furthermore, the fact that we find a link on a given domain does not mean that we actively monitor that domain. For example, we may find a blog post on `blog.example.com` via a pre-aggregated RSS feed. That does not mean that we necessarily monitor the news feed of `blog.example.com`.

## Domain Decision Structure

Q: How to get the most recent version of the domain decision list?

Go to `https://artifact.eventdata.crossref.org/a/artifacts.json`, look up the value at `artifacts.domain-decision-structure.current-version-link`. Follow the URL in the value. There is no direct URL to go straight to the most recent version.



