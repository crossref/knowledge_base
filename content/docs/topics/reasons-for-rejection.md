---
desc: ''
products: []
related_services: []
repo_links: []
tags: null
title: Reasons for Rejection of a Deposit
weight: 0
---

 - Deposit XML schema validation - if invalid, the deposit is rejected.
 - Prefix ownership check for the depositor id. If failed, the deposit is rejected.
 - Title validation, i.e. title metadata consistency check. If fails, the deposit is rejected. For example, we check if the title is already associated with a different ISSN/DOI, or whether the depositor is the owner of the title (ownership is determined based on who was the first to deposit the title).
    - Note that “title” here means not the title of the item, but the title of its container (journal name/title, conference name, book title, etc.)
    - This check is not fuzzy. There is only some basic normalization such as lower case and punctuation removed.
 - If the DOI is already present in the system, we check the timestamp in the deposit. If it is not greater than the previous timestamp of this DOI, the deposit is rejected.
     - Note that “timestamp” here does not have to be related to any time/date. This is in fact a version number (integer). Of course, it can be a proper timestamp, if the publisher decides so (and many do), but we do not require that.
     - This check was introduced to prevent publishers from overwriting data with old data. It basically signals that the deposit is intentional and not sent by accident.
 - Pending publication validation
 - If the DOI's Resource URL is equal to the DOI (i.e. the DOI redirects to itself in a loop), it will be rejected. The `org.crossref.ds.metadata.structs.DOIData` checks for combinations of HTTP, HTTPS, dx.doi.org or doi.org . The error is raised as a `DsException`. 