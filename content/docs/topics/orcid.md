---
desc: ''
products: []
related_services: []
repo_links: []
tags:
- orcid
title: ORCID In Crossref
weight: 0
---

ORCID has some basic validation for the Publication Year of works being added to ORCID records. The publication year must be between 1900 and 2100. Given this, if the work being registered or updated in Crossref contains 1 or more ORCID IDs and has any of the publication dates outside the range 1900 - 2100 we  fail the item being submitted, and alert the user accordingly, otherwise we will receive erros when trying to autoupdate the ORCID profile with that work.

`Publication year [year] is not valid for a work that contains an ORCID`

ORCID is used in the following Crossref services:

 - Crossref Metadata Search
 - ORCID Auto-update
 
See services linked from the ORCID tag above.
 
